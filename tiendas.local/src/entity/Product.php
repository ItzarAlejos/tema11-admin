<?php
/**
 * Created by PhpStorm.
 * User: daw2
 * Date: 30/01/19
 * Time: 18:39
 */

namespace ProyectoWeb\entity;

class Product extends Entity
{
    const RUTA_IMAGENES = "/img/";
    const RUTA_IMAGENES_CARRUSEL = "/img/imgcarrusel/";
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $nombre;
    /**
     * @var string
     */
    protected $descripcion;
    /**
     * @var int
     */
    protected $id_categoria;
    /**
     * @var double
     */
    protected $precio;
    /**
     * @var string
     */
    protected $foto;
    /**
     * @var int
     */
    protected $destacado;
    /**
     * @var string
     */
    protected $carrusel;

    /**
     * Productos constructor.
     * @param int $id
     * @param string $nombre
     * @param string $descripcion
     * @param int $id_categoria
     * @param float $precio
     * @param string $foto
     * @param int $destacado
     * @param string $carrusel
     */
    public function __construct(int $id = null,
                                string $nombre = '',
                                string $descripcion="",
                                int $id_categoria= null,
                                float $precio = null,
                                string $foto ="",
                                int $destacado = null,
                                string $carrusel= "")
    {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->descripcion = $descripcion;
        $this->id_categoria = $id_categoria;
        $this->precio = $precio;
        $this->foto = $foto;
        $this->destacado = $destacado;
        $this->carrusel = $carrusel;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Product
     */
    public function setId(int $id): Product
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Product
     */
    public function setNombre(string $nombre): Product
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescripcion(): string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     * @return Product
     */
    public function setDescripcion(string $descripcion): Product
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdCategoria(): int
    {
        return $this->id_categoria;
    }

    /**
     * @param int $id_categoria
     * @return Product
     */
    public function setIdCategoria(int $id_categoria): Product
    {
        $this->id_categoria = $id_categoria;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrecio(): float
    {
        return $this->precio;
    }

    /**
     * @param float $precio
     * @return Product
     */
    public function setPrecio(float $precio): Product
    {
        $this->precio = $precio;
        return $this;
    }

    /**
     * @return string
     */
    public function getFoto(): string
    {
        return $this->foto;
    }

    /**
     * @param string $foto
     * @return Product
     */
    public function setFoto(string $foto): Product
    {
        $this->foto = $foto;
        return $this;
    }

    /**
     * @return int
     */
    public function getDestacado(): int
    {
        return $this->destacado;
    }

    /**
     * @param int $destacado
     * @return Product
     */
    public function setDestacado(int $destacado): Product
    {
        $this->destacado = $destacado;
        return $this;
    }
    /**
     * @return string
     */
    public function getCarrusel(): string
    {
        return $this->carrusel;
    }

    /**
     * @param string $carrusel
     * @return Product
     */
    public function setCarrusel(string $carrusel): Product
    {
        $this->carrusel = $carrusel;
        return $this;
    }

    public function toArray(): array
    {
        return [

            'id' => $this->getId(),
            'nombre' => $this->getNombre(),
            'descripcion' => $this->getDescripcion(),
            'id_categoria' => $this->getIdCategoria(),
            'precio' => $this->getPrecio(),
            'foto' => $this->getFoto(),
            'destacado' => $this->getDestacado(),
            'carrusel' => $this->getCarrusel()
        ];



    }
}