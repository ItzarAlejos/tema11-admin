<?php
/**
 * Created by PhpStorm.
 * User: daw2
 * Date: 25/01/19
 * Time: 17:01
 */

namespace ProyectoWeb\entity;


class Category extends Entity
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $icon;
    
    /**
     * @param string $nombre
     * @param string $logo
     * @param string $descripcion
     */
    public function __construct(int $id = null, string $nombre = '', string $icon = ''){
        $this->id = $id;
        $this->nombre = $nombre;
        $this->icon = $icon;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Category
     */
    public function setNombre(string $nombre): Category
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     * @return Category
     */
    public function setIcon(string $icon): Category
    {
        $this->icon = $icon;
        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'nombre' => $this->getnombre(),
            'icon' => $this->getIcon()
        ];
    }
}