<?php
/**
 * Created by PhpStorm.
 * User: daw2
 * Date: 25/01/19
 * Time: 17:02
 */

namespace ProyectoWeb\repository;


use ProyectoWeb\database\QueryBuilder;

class CategoryRepository extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct("categorias","Category");
    }
}